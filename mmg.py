import math
from mcg import mcg
from pearson import pearson_test 
from kolmogorov import kolmogorov_test 

def mmg(s1, s2, k):
    """McLaren-Marsaglia generator."""
    table = s1[0:k]
    result = []
    for i in range(len(s1)):
        j = int(s2[i] * k)
        result.append(table[j])
        table[j] = s1[(i + k) % len(s1)]

    return result

if __name__ == "__main__":
    a0 = 16807
    b = a0
    K = 64
    mod = 2**31

    s1 = [x for x in mcg(mod, b, a0)]
    s2 = [x for x in mcg(mod, b*2 + 1, a0 + 1)]
    s3 = mmg(s1, s2, K)
    print(s3)
    print(kolmogorov_test(s3))
    print(pearson_test(s3, 64))

