def lcg(modulus, a, c, seed, count=100):
    """Linear congruential generator."""
    for i in range(count):
        seed = (a * seed + c) % modulus
        yield seed / modulus

def mcg(modulus, a, seed, count=100):
    """Multiplicative congruential generator."""
    yield from lcg(modulus, a, 0, seed, count)

if __name__ == '__main__':
    for i in mcg(2**31, 16807, 16807):
        print(i)
