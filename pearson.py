def pearson_test(seq, k):
    seq.sort()
    
    n = len(seq)
    chi2 = 0
    j = 0
    for i in range(1, k):
        freq = 0
        while j < n and seq[j] < i / k:
            j += 1
            freq += 1
        chi2 += (freq - n/k) ** 2 / (n/k)

    print(chi2)
    return chi2 < 18.3
