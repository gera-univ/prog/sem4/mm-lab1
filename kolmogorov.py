import math

def kolmogorov_test(seq):
    seq.sort()

    n = len(seq)

    supr = 0
    for i in range(n):
        if supr < abs((i + 1)/n - seq[i]):
            supr = seq[i]

    return math.sqrt(n) * supr < 1.36
